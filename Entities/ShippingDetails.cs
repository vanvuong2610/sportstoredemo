﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Entities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage = "Vui lòng nhập họ tên")]
        [StringLength(100, ErrorMessage = "Họ tên không được quá 100 ký tự")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Vui lòng nhập địa chỉ")]        
        public string Address { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên thành phố")]
        public string City { get; set; }
        
        [Required(ErrorMessage = "Vui lòng nhập địa chỉ email")]
        [EmailAddress(ErrorMessage = "Email không đúng định dạng")]
        public string Email { get; set; }         

        [Required(ErrorMessage = "Vui lòng nhập số điện thoại")]
        public string PhoneNumber { get; set; }

        public bool GiftWrap { get; set; }
        
    }
}

