﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ProductApiModel
    {
        public int? ProductId { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên sản phẩm")]
        [StringLength(200, ErrorMessage = "Tên sản phẩm chỉ gồm 200 ký tự")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Vui lòng cung cấp giá sản phẩm")]        
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Vui lòng cung cấp số lượng sản phẩm")]
        public int Amount { get; set; }

        [Required(ErrorMessage = "Vui lòng cung cấp loại sản phẩm")]
        public int CategoryId { get; set; }
    }
}