﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Abstract;
using WebApplication1.Entities;

namespace WebApplication1.Concrete
{
    public class ProductRepository : IProductRepository
    {
        private SportStoreEntities context = new SportStoreEntities();
        public IQueryable<Category> Categories
        {
            get
            {
                return context.Categories;
            }
        }

        public IQueryable<Product> Products
        {
            get
            {
                return context.Products;
            }
        }

        public void Save(OrderDetail orderDetail)
        {
            var tmp = context.OrderDetails
                .FirstOrDefault(x => x.ProductId == orderDetail.ProductId && x.OrderId == orderDetail.OrderId);
            if(tmp == null)
            {
                //insert
                context.OrderDetails.Add(orderDetail);
            }
            else
            {
                //update
                tmp.OrderPrice = orderDetail.OrderPrice;
                tmp.Quantity = orderDetail.Quantity;
            }
            //commit data
            context.SaveChanges();
        }

        public void Save(Product product)
        {
            var tmp = context.Products.FirstOrDefault(x => x.ProductId == product.ProductId);
            if(tmp == null)
            {
                //insert
                context.Products.Add(product);
            }
            else
            {
                //update
                tmp.Amount = product.Amount;
                tmp.CategoryId = product.CategoryId;
                tmp.Price = product.Price;
                tmp.ProductName = product.ProductName;
            }
            //commit data
            context.SaveChanges();
        }

        public void Save(Order order)
        {
            var tmp = context.Orders.FirstOrDefault(x => x.OrderId == order.OrderId);
            if(tmp == null)
            {
                //insert                
                context.Orders.Add(order);
            }
            else
            {
                //update
                tmp.CustomerId = order.CustomerId;
                tmp.GiftWrap = order.GiftWrap;
                tmp.OrderDate = order.OrderDate;                
            }
            //commit data
            context.SaveChanges();
        }

        public void Save(Customer customer)
        {
            var tmp = context.Customers.FirstOrDefault(x => x.CustomerId == customer.CustomerId);
            if(tmp == null)
            {
                //insert
                context.Customers.Add(customer);
            }
            else
            {
                //update
                tmp.Address = customer.Address;
                tmp.CustomerName = customer.CustomerName;
                tmp.Email = customer.Email;
                tmp.Phone = customer.Phone;                
            }
            //commit data
            context.SaveChanges();
        }
    }
}

