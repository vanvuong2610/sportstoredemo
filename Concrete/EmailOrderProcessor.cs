﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using WebApplication1.Abstract;
using WebApplication1.Entities;

namespace WebApplication1.Concrete
{
    public class EmailOrderProcessor : IOrderProcessor
    {
        private IProductRepository productRepository { get; set; }
        public EmailOrderProcessor(IProductRepository repo)
        {
            productRepository = repo;
        }
        public void ProcessOrder(Cart cart, ShippingDetails shippingDetails)
        {
            //Lưu thông tin khách hàng
            Customer customer = new Customer()
            {
                Address = shippingDetails.Address,
                CustomerName = shippingDetails.Name,
                Email = shippingDetails.Email,
                Phone = shippingDetails.PhoneNumber,
                CustomerId = 0
            };
            productRepository.Save(customer);

            //Lưu thông tin đơn hàng
            Order order = new Order()
            {
                CustomerId = customer.CustomerId,
                GiftWrap = shippingDetails.GiftWrap,
                OrderDate = DateTime.Now,
                OrderId = 0
            };
            productRepository.Save(order);

            //Lưu chi tiết đơn hàng
            OrderDetail orderDetail = null;
            foreach(var detail in cart.Lines)
            {
                orderDetail = new OrderDetail()
                {
                    OrderId = order.OrderId,
                    OrderPrice = detail.Product.Price,
                    ProductId = detail.Product.ProductId,
                    Quantity = detail.Quantity
                };
                productRepository.Save(orderDetail);
            }            
            //tạo đối tượng chứa nội dung email
            EmailMessage message = new EmailMessage();

            message.To = shippingDetails.Email;
            message.From = ConfigurationManager.AppSettings["email"];
            message.Subject = string.Format("Thông tin đơn hàng #{0}", order.OrderId);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<h1>Kính chào Quý khách: {0}</h1>", shippingDetails.Name);
            sb.Append("<p>Quý khách vừa đặt hàng tại website http://sportstore.vn, thông tin cụ thể như sau:</p>");
            sb.Append("<table style='width:100%; border: 1px solid #000'>");
            sb.Append("<tr>");
            sb.Append("<td>Tên hàng</td>");
            sb.Append("<td>Số lượng</td>");            
            sb.Append("<td>Đơn giá</td>");
            sb.Append("<td>Thành tiền</td>");
            sb.Append("</tr>");
            foreach(var item in cart.Lines)
            {
                sb.Append("<tr>");
                sb.AppendFormat("<td>{0}</td>", item.Product.ProductName);
                sb.AppendFormat("<td>{0}</td>", item.Quantity);
                sb.AppendFormat("<td>{0}</td>", item.Product.Price);
                sb.AppendFormat("<td>{0}</td>", item.Product.Price * item.Quantity);
                sb.Append("</tr>");
            }
            sb.Append("<tr>");
            sb.AppendFormat("<td colspan='4'>Tổng tiền: {0}</td>", cart.ComputeTotalValue());
            sb.Append("</tr>");
            sb.Append("</table>");

            sb.Append("<hr/>");
            sb.AppendFormat("<p>Địa chỉ: {0}</p>", shippingDetails.Address);
            sb.AppendFormat("<p>Email: {0}</p>", shippingDetails.Email);
            sb.AppendFormat("<p>Điện thoại: {0}</p>", shippingDetails.PhoneNumber);
            sb.Append("<p>Trân trọng cảm ơn</p>");

            message.Body = sb.ToString();
            
            //tạo đối tượng gửi mail
            EmailHelper sender = new EmailHelper();
            sender.Send(message);
        }
    }
}