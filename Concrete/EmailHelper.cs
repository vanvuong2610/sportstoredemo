﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace WebApplication1.Concrete
{
    public class EmailHelper
    {
        public void Send(EmailMessage message)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.EnableSsl = true;
                smtpClient.EnableSsl = true;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials
                    = new NetworkCredential("vanvuong2610@gmail.com", "qhfspggbkmmtiujp");
                
                MailMessage mailMessage = new MailMessage(
                                       message.From,
                                       message.To,
                                       message.Subject,
                                       message.Body);
                if(message.CC != null)
                {
                    foreach(var cc in message.CC)
                    {
                        mailMessage.CC.Add(cc);
                    }
                }

                if(message.BCC != null)
                {
                    foreach(var bcc in message.BCC)
                    {
                        mailMessage.Bcc.Add(bcc);
                    }
                }
                mailMessage.IsBodyHtml = true;
                smtpClient.Send(mailMessage);
            }
        }
    }

    public class EmailMessage
    {
        public string From { get; set; }
        public string To { get; set; }
        public List<string> CC { get; set; }
        public List<string> BCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}