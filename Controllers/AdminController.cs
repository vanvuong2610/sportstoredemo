﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Abstract;
using WebApplication1.Entities;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private IProductRepository productRepository { get; set; }
        public AdminController(IProductRepository productRepo)
        {
            productRepository = productRepo;
        }
        // GET: Admin
        public ViewResult ProductList(int page = 1)
        {
            int pageSize = 1;

            ProductIndexViewModel model = new ProductIndexViewModel();
            model.Products = productRepository.Products
                .OrderByDescending(x => x.ProductId)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            model.PagingInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = productRepository.Products.Count()
            };

            var userName = User.Identity.Name;

            return View(model);
        }

        public ViewResult ProductAdd()
        {
            //Sử dụng ViewBag để truyền dữ liệu từ controller sang view
            ViewBag.Categories = productRepository.Categories.ToList();

            return View();
        }

        public ViewResult ProductEdit(int id)
        {
            ViewBag.Categories = productRepository.Categories.ToList();

            Product product = productRepository
                .Products
                .FirstOrDefault(x => x.ProductId == id);

            return View(product);
        }

        public ActionResult ProductDelete(int id)
        {
            return RedirectToAction("ProductList");
        }

        [HttpPost]
        public ActionResult ProductAdd(Product model)
        {
            if (ModelState.IsValid)
            {
                //lưu thông tin sản phẩm vào cơ sở dữ liệu
                productRepository.Save(model);
                //truyền thông báo lên view
                TempData["msg"] = "Thêm mới sản phẩm thành công";
                //chuyển về trang danh sách sản phẩm
                return RedirectToAction("ProductList");
            }
            else
            {
                TempData["msg"] = "Vui lòng cung cấp đủ thông tin sản phẩm";
                //Tạo lại danh sách danh mục sản phẩm
                ViewBag.Categories = productRepository.Categories.ToList();
                //Truyền lại dữ liệu dữ liệu người dùng nhập trên view ProductAdd
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult ProductEdit(Product model)
        {
            if (ModelState.IsValid)
            {
                //lưu thông tin sản phẩm vào cơ sở dữ liệu
                productRepository.Save(model);
                //truyền thông báo lên view
                TempData["msg"] = "Cập nhật sản phẩm thành công";
                //chuyển về trang danh sách sản phẩm
                return RedirectToAction("ProductList");
            }
            else
            {
                TempData["msg"] = "Vui lòng cung cấp đủ thông tin sản phẩm";
                //Tạo lại danh sách danh mục sản phẩm
                ViewBag.Categories = productRepository.Categories.ToList();
                //Truyền lại dữ liệu dữ liệu người dùng nhập trên view ProductEdit
                return View(model);
            }
        }
    }
}