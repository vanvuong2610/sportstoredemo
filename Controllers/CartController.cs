﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Abstract;
using WebApplication1.Entities;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class CartController : Controller
    {
        private IProductRepository productRepository;
        private IOrderProcessor orderProcessor;

        public CartController(IProductRepository repo, IOrderProcessor orderPara)
        {
            productRepository = repo;
            orderProcessor = orderPara;
        }
        
        public RedirectToRouteResult AddToCart(Cart cart, int productId, string returnUrl)
        {
            Product product = productRepository.Products
                .FirstOrDefault(p => p.ProductId == productId);

            if (product != null)
            {
                cart.AddItem(product, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
        {
            Product product = productRepository.Products
                .FirstOrDefault(p => p.ProductId == productId);

            if (product != null)
            {
                cart.RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public PartialViewResult SummaryCart(Cart cart)
        {
            return PartialView(cart);
        }

        public ViewResult Index(Cart cart, string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        public ActionResult UpdateQuantity(Cart cart, int productId , int quantity, string returnUrl)        
        {
            cart.UpdateQuantityByProductID(new Product { ProductId = productId }, quantity);
            return RedirectToAction("Index", new { returnUrl });
        }

        public ViewResult Checkout()
        {
            return View(new ShippingDetails());
        }

        [HttpPost]
        public ActionResult Checkout(Cart cart, ShippingDetails shippingDetails)
        {
            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Vui lòng chọn thêm sản phẩm");
            }

            if (ModelState.IsValid)
            {
                //Xử lý đơn hàng: Lưu vào CSDL, gửi email
                orderProcessor.ProcessOrder(cart, shippingDetails);
                //xóa thông tin giỏ hàng
                cart.Clear();
                //chuyển người dùng đến view thông báo hoàn tất đặt hàng
                return View("Completed");
            }
            else
            {
                return View(shippingDetails);
            }
        }        
    }
}

