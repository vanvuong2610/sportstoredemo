﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ViewResult Index()
        {
            List<Customer> model = new List<Customer>();
            model.Add(new Customer()
            {
                CustomerId = 1,
                Address = "Đà Nẵng",
                CustomerName = "Nguyễn Văn Vương",
                Email = "vanvuong2610@gmail.com",
                PhoneNumber = "0977 678 434"
            });
            model.Add(new Customer()
            {
                CustomerId = 2,
                Address = "Đà Nẵng",
                CustomerName = "Đỗ Minh Tuấn",
                Email = "it.dominhtuan@gmail.com",
                PhoneNumber = "0935 907 813"
            });
            return View(model);
        }
    }
}