﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Abstract;
using WebApplication1.Concrete;
using WebApplication1.Entities;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ProductServiceController : ApiController
    {
        private IProductRepository productRepository = new ProductRepository();
        
        public List<ProductApiModel> Get()
        {
            var products = productRepository.Products.ToList();

            List<ProductApiModel> model = new List<ProductApiModel>();
            products.ForEach(x => model.Add(new ProductApiModel()
            {
                Amount = x.Amount,
                CategoryId = x.CategoryId,
                Price = x.Price,
                ProductId = x.ProductId,
                ProductName = x.ProductName
            }));

            return model;
        }

        [HttpPost]
        public bool Post(ProductApiModel model)
        {
            if (ModelState.IsValid)
            {
                productRepository.Save(new Product()
                {
                    Amount = model.Amount,
                    CategoryId = model.CategoryId,
                    Price = model.Price,
                    ProductId = model.ProductId.GetValueOrDefault(0),
                    ProductName = model.ProductName
                });
                return true;
            }
            return false;
        }        
    }
}
