﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Abstract;
using WebApplication1.Concrete;
using WebApplication1.Entities;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository productRepository { get; set; }

        public ProductController(IProductRepository productRepoPara)
        {
            productRepository = productRepoPara;
        }

        public ViewResult Index(int page = 1, int categoryId = 0)
        {
            int pageSize = 1;

            ProductIndexViewModel model = new ProductIndexViewModel();
            model.Products = productRepository.Products
                .Where(x => categoryId == 0 || x.CategoryId == categoryId)
                .OrderByDescending(x=>x.ProductId)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            model.PagingInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = productRepository.Products
                .Where(x=> categoryId == 0 || x.CategoryId == categoryId).Count()
            };

            model.CurrentCategory = categoryId;

            return View(model);
        }

        public ViewResult Detail(int productId)
        {
            Product product = productRepository
                .Products
                .FirstOrDefault(x => x.ProductId == productId);

            if(product == null)
            {
                return View("NotFound");
            }

            return View(product);
        }

        public PartialViewResult Navigation()
        {
            List<Category> categories = productRepository.Categories.ToList();
            return PartialView(categories);
        }


    }
}