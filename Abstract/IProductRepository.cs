﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Entities;

namespace WebApplication1.Abstract
{
    public interface IProductRepository
    {
        IQueryable<Product> Products { get; }
        IQueryable<Category> Categories { get; }

        void Save(Customer customer);
        void Save(Order order);
        void Save(OrderDetail orderDetail);

        void Save(Product product);
    }
}


